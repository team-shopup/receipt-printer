const puppeteer = require('puppeteer');
const getPixels = require('get-pixels');
const { Image, Network, Printer } = require('escpos');
const moment = require('moment-timezone');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const APP_PORT = 4005;

let browser;
let page;

const getHtml = ({ 
    date = moment().tz('Asia/Dhaka').format('DD-MMM-YYYY dddd'), 
    time = moment().tz('Asia/Dhaka').format('h:mm A'), 
    shopName, 
    sellerPhone,
    customerName, 
    customerPhone, 
    deliveryAddress,
    cash,
    sellerInstruction = '', 
    area, 
    hubName = '',
    partnerName = ''
  }) => (`
  <html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
      <div id="print">
        <h1 class="title">ShopUp</h1>
        <div class="date-time">
          <span>Date: ${date}</span>
          <span>Time: ${time}</span>
        </div>
        <div class="seller">
          <p>${shopName}</p>
          <p>${sellerPhone}</p>
        </div>
        <div class="shopper">
          <p>${customerName}</p>
          <p>${customerPhone}</p> 
          <p>${deliveryAddress}</p>
        </div>
        <div class="cash">
          <span>Amount:</span>
          <span>৳${cash}/=</span>
        </div>
        <div class="note">
          <p>Seller Note: ${sellerInstruction || 'none'}</p>
        </div>
        <div class="area-hub">
          <span>Area: ${area}</span>
          <span>Hub: ${hubName || partnerName}</span>
        </div>
      </div>
    </body>
    <style>
      #print {
        border: 1px dashed black;
        padding: 10px;
        width: 100%;
        max-width: 553px;
        font-size: 25px;
        -webkit-font-smoothing: antialiased;
        font-family: 'sans-serif';
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
      }
      .title {
        margin: 0;
        padding: 0;
        padding-bottom: 5px;
        font-family: Roboto, 'sans-serif'
      }
      .date-time {
        width: 100%;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        border-top: 1px dashed black;
        border-bottom: 1px dashed black;
        padding: 5px 0;
      }
      .seller, .shopper, .note, .cash {
        width: 100%;
        border-bottom: 1px dashed black;
        padding: 5px 0;
      }
      .shopper > p, .seller > p {
        margin: 3px 0;
      }
      .cash {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        align-items: center;
      }
      .area-hub {
        width: 100%;
        padding-top: 3px;
        display: flex;
        flex-direction: row;
        justify-content: space-between;
      }
    </style>
  </html>
`);

app.use(cors());

app.use(bodyParser.json());

app.post('/print', async (req, res) => {
  console.log("Printing recript for the following info: ", req.body);

  if (!req.body.printer_ip) {
    console.error('No IP address given');
    return res.status(400).json({ success: false, message: "No IP address given" });
  }

  try {
    const html = getHtml(req.body);
    await page.setContent(html);
    const el = await page.$('#print');
    const screenShot = await el.screenshot();

    getPixels(screenShot, 'image/png', async function(err, pixels) {
      if (err) throw err;
      const image = new Image(pixels);
      console.log((image instanceof Image));
      
      const device  = new Network(req.body.printer_ip);
      const options = { encoding: "utf8" };
      const printer = new Printer(device, options);

      device.open(async function(){
        await printer.align('ct').raster(image);
        await printer.text("").text("")
          .barcode(req.body.id, 'CODE39', { width: 1 })
          .text("").text("")
          .cut().close();
          res.status(200).json({ success: true, message: "Printing successful" });
      });
    });
  } catch (err) {
    console.error(err);
    res.status(400).json({ success: false, message: "Printing failed", err });
  }
});

app.listen(APP_PORT, async () => {
  browser = await puppeteer.launch({
    headless: true,
    args: [
    '--incognito',
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--shm-size 1G',
    '--enable-logging'
    ],  
  });
  page = await browser.newPage();
  await page.setViewport({
    width: 640,
    height: 480,
    deviceScaleFactor: 1
  });  
  console.log(`Example app listening on port ${APP_PORT}!`); 
});